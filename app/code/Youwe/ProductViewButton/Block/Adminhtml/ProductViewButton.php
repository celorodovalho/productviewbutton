<?php

namespace Youwe\ProductViewButton\Block\Adminhtml;


class ProductViewButton extends \Magento\Backend\Block\Widget\Container
{
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * App Emulator
     *
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $emulation;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Store\Model\App\Emulation $emulation
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product $product,
        \Magento\Store\Model\App\Emulation $emulation,
        array $data = []
    ) {

        $this->coreRegistry = $registry;
        $this->product = $product;
        $this->_request = $context->getRequest();
        $this->emulation = $emulation;
        parent::__construct($context, $data);
    }

    /**
     * Block constructor adds buttons
     */
    protected function _construct()
    {
        $this->addButton(
            'preview_product',
            $this->getButtonData()
        );

        parent::_construct();
    }


    /**
     * Return button attributes array
     */
    public function getButtonData()
    {
        return [
            'label' => __('View'),
            'on_click' => sprintf("window.open('%s')", $this->getProductUrl()),
            'class' => 'view disable action-secondary',
            'sort_order' => 20
        ];
    }

    /**
     * Return product frontend url depends on active store
     * @return mixed
     */
    public function getProductUrl()
    {
        $store = $this->_request->getParam('store');
        if (!$store) {
            $this->emulation->startEnvironmentEmulation(
                null,
                \Magento\Framework\App\Area::AREA_FRONTEND,
                true
            );
            $productUrl = $this->product->loadByAttribute(
                'entity_id',
                $this->coreRegistry->registry('product')->getId()
            )->getProductUrl();
            $this->emulation->stopEnvironmentEmulation();

            return $productUrl;
        } else {
            return $this->product
                ->loadByAttribute(
                    'entity_id',
                    $this->coreRegistry->registry('product')->getId()
                )->setStoreId($store)->getUrlInStore();
        }
    }
}