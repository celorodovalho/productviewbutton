require([
    'jquery',
    'Magento_Ui/js/modal/alert'
], function ($, alert) {
    'use strict';
    $(document).ready(function () {
        var buttonElement = $('#preview_product'),
            inlineEvent = buttonElement.prop("onclick");
        buttonElement.prop("onclick", null).off("click").on('click', function (e) {
            if (window.productHasChange) {
                e.preventDefault();
                alert({
                    title: 'Attention',
                    content: 'You need to save the changes before proceed.',
                    actions: {
                        always: function () {
                        }
                    }
                });
                return false;
            } else {
                inlineEvent();
            }
        });
    });

});