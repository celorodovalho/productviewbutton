define(
    [
        'underscore',
        'uiComponent',
        'ko',
        'jquery'
    ],
    function (_, Component, ko, $) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    synchronous: true,
                    productData: {},
                    imports: {
                        productData: '${ $.provider }:data'
                    },
                },

                /**
                 * execution starts
                 */
                initialize: function () {
                    var self = this;
                    this._super();
                    window.productHasChange = false;
                    self.initSubscribers();
                },

                /**
                 * init observers
                 */
                initObservable: function () {
                    this._super().observe('productData');
                    return this;
                },

                /**
                 * initialize subscribers
                 */
                initSubscribers: function () {
                    var self = this;
                    var origForm = JSON.stringify(self.productData());
                    //creating a knockout subscriber to observe any changes in the product object
                    self.productData.subscribe(
                        function (productData) {
                            var productJson = JSON.stringify(productData);
                            var spinner = $('#container .spinner:visible');
                            if (spinner.length) {
                                origForm = productJson;
                            }
                            if (origForm !== productJson) {
                                window.productHasChange = true;
                            }
                        }
                    );
                },
            }
        );
    }
);