<?php

namespace Youwe\ProductViewButton\Test\Unit\Block\Adminhtml\ProductViewButtonTest;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class ProductViewButtonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Youwe\ProductViewButton\Block\Adminhtml\ProductViewButton
     */
    protected $model;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $productMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $urlMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registryMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $emulationMock;

    protected function setUp()
    {
        $this->productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $this->productMock->expects($this->any())->method('getId')->willReturn(1);
        $this->productMock->expects($this->any())->method('loadByAttribute')->willReturn($this->productMock);
        $this->productMock->expects($this->any())->method('getProductUrl')->willReturn('test');

        $buttonList = $this->getMockBuilder('\Magento\Backend\Block\Widget\Button\ButtonList')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $buttonList->expects($this->once())->method('add')->willReturn(0);

        $this->registryMock = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $this->registryMock->expects($this->any())->method('registry')->willReturn($this->productMock);

        $this->requestMock = $this->getMockForAbstractClass(
            'Magento\Framework\App\RequestInterface',
            [],
            '',
            false,
            true,
            true,
            ['getParam']
        );
        $this->requestMock->expects($this->any())->method('getParam')->willReturn(0);

        $this->emulationMock = $this->getMockBuilder('\Magento\Store\Model\App\Emulation')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);

        $contextMock = $this->getMockBuilder('\Magento\Backend\Block\Widget\Context')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $contextMock->expects($this->any())->method('getButtonList')->willReturn($buttonList);

        $this->model = (new ObjectManager($this))->getObject(
            'Youwe\ProductViewButton\Block\Adminhtml\ProductViewButton',
            [
                'context' => $contextMock,
                'registry' => $this->registryMock,
                'product' => $this->productMock,
                'request' => $this->requestMock,
                'emulation' => $this->emulationMock
            ]
        );
    }

    protected function setUpStore()
    {
        $this->productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $this->productMock->expects($this->any())->method('getId')->willReturn(1);
        $this->productMock->expects($this->any())->method('loadByAttribute')->willReturn($this->productMock);
        $this->productMock->expects($this->any())->method('getProductUrl')->willReturn('test');
        $this->productMock->expects($this->any())->method('setStoreId')->willReturn($this->productMock);
        $this->productMock->expects($this->any())->method('getUrlInStore')->willReturn('test');

        $buttonList = $this->getMockBuilder('\Magento\Backend\Block\Widget\Button\ButtonList')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $buttonList->expects($this->once())->method('add')->willReturn(0);

        $this->registryMock = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $this->registryMock->expects($this->any())->method('registry')->willReturn($this->productMock);

        $this->requestMock = $this->getMockForAbstractClass(
            'Magento\Framework\App\RequestInterface',
            [],
            '',
            false,
            true,
            true,
            ['getParam']
        );
        $this->requestMock->expects($this->any())->method('getParam')->willReturn(1);

        $this->emulationMock = $this->getMockBuilder('\Magento\Store\Model\App\Emulation')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);

        $contextMock = $this->getMockBuilder('\Magento\Backend\Block\Widget\Context')
            ->disableOriginalConstructor()
            ->getMock([], [], '', false);
        $contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $contextMock->expects($this->any())->method('getButtonList')->willReturn($buttonList);

        $this->model = (new ObjectManager($this))->getObject(
            'Youwe\ProductViewButton\Block\Adminhtml\ProductViewButton',
            [
                'context' => $contextMock,
                'registry' => $this->registryMock,
                'product' => $this->productMock,
                'request' => $this->requestMock,
                'emulation' => $this->emulationMock
            ]
        );
    }

    public function testGetButtonData()
    {
        $this->setUpStore();
        $data = [
            'label' => __('View'),
            'class' => 'view disable action-secondary',
            'on_click' => 'window.open(\'test\')',
            'sort_order' => 20,
        ];
        $this->assertEquals($data, $this->model->getButtonData());
    }

    public function testGetButtonDataStore()
    {
        $data = [
            'label' => __('View'),
            'class' => 'view disable action-secondary',
            'on_click' => 'window.open(\'test\')',
            'sort_order' => 20,
        ];
        $this->assertEquals($data, $this->model->getButtonData());
    }

    public function testGetProductUrl()
    {
        $data = 'test';
        $this->assertEquals($data, $this->model->getProductUrl());
    }

    public function testGetProductUrlStore()
    {
        $this->setUpStore();
        $data = 'test';
        $this->assertEquals($data, $this->model->getProductUrl());
    }
}
